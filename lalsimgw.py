# --- lalsimgw.py ---

"""
	User-friendly methods to call LAL waveform generation routines 
	without needing detailed knowledge of these codes and their conventions.
"""

__author__ = "Evan Ochsner <evano@gravity.phys.uwm.edu> and Chad Galley <crgalley@gmail.com>"

import lal, lalsimulation as lalsim
import numpy as np
#import re
import gwtools


##############################################
class Setup:
	
	#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	def __init__(self):
		self.fref = 0.
		self.lambda1 = 0.
		self.lambda2 = 0.
		self.waveFlags = None
		self.nonGRparams = None
		self.ampO = 0
		self.phaseO = 7
		
		self.htype_options = {
			'hp',
			'hc',
			'hphc', 
			'h', 
			'Amp',
			'Phase',
			'PhaseAmp'
		} 
		
		pass
	
	#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	def time(self, num):
		"""Make time array given number of samples"""
		return np.arange(0, self.dt*num, self.dt)
		
	#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	def freq(self, num):
		"""Make frequency array given number of samples"""
		return np.linspace(self.fmin, self.fmin+num*self.df, num=num)
	
	#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	def taper(self, hp, hc):
		"""Taper start of time-domain waveform"""
		lalsim.SimInspiralREAL8WaveTaper(hp, lalsim.SIM_INSPIRAL_TAPER_START)
		lalsim.SimInspiralREAL8WaveTaper(hc, lalsim.SIM_INSPIRAL_TAPER_START)
		pass
	
	#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	def lal_fft(self, hp, hc, sample, df, fmin):
		
		# Find next power of 2 length
		padlen = 1
		while padlen < hp.data.length and padlen < hc.data.length:
			padlen *= 2
		
		# If df=0, use above padlen. If df != 0, then we...
		if df != 0:
			# Check if padlen gives smaller df than requested
			# If so, print warning and use padlen
			if padlen > int(sample/df):
				print "Warning: waveform too long for requested df =", df
				print "Instead using df =", padlen/sample
			# If requested df is finer freq. binning, then pad further
			if padlen < int(sample/df):
				padlen = int(sample/df)
			
		# Pad hp and hc
		lal.ResizeREAL8TimeSeries(hp, 0, padlen)
		lal.ResizeREAL8TimeSeries(hc, 0, padlen)
		
		self.F = padlen/2+1
		self.df = df
		self.fmin = fmin
				
		# FFT to get the FD waveform
		plan = lal.CreateForwardREAL8FFTPlan(padlen, 0)
		hpoff = lal.CreateCOMPLEX16FrequencySeries("FD hplus", hp.epoch, 0, 1, lal.HertzUnit, padlen/2+1)
		hcoff = lal.CreateCOMPLEX16FrequencySeries("FD hcross", hc.epoch, 0, 1, lal.HertzUnit, padlen/2+1)
		lal.REAL8TimeFreqFFT(hpoff, hp, plan)
		lal.REAL8TimeFreqFFT(hcoff, hc, plan)
		
		return hpoff, hcoff
	
	#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	def return_waveform(self, hp, hc, htype):
		"""Return waveform components from LAL data types.
		
		Input:
		------
			hp		-- Plus polarization REAL8TimeSeries or COMPLEX16FrequencySeries data 
			hc		-- Cross polarization REAL8TimeSeries or COMPLEX16FrequencySeries data
			htype 	-- Specify which waveform component desired (see below)
		
		Options for htype:
		------------------
			'hp' 		-- Plus polarization only 
			'hc' 		-- Cross polarization only 
			'hphc' 		-- Plus and cross polarizations
			'h' 		-- Complex waveform, hp+1j*hc
			'Amp' 		-- Waveform amplitude only
			'Phase' 	-- Waveform phase only 
			'PhaseAmp' 	-- Waveform phase and amplitude
		"""
		
		if htype == 'hp':
			ans = hp
		elif htype == 'hc':
			ans = hc
		elif htype == 'hphc':
			ans = np.array([hp, hc])
		elif htype == 'h':
			ans = hp - 1j*hc
		elif htype == 'Amp':
			ans = np.abs(hp - 1j*hc)
		elif htype == 'Phase':
			ans = gwtools.phase(hp - 1j*hc)
		elif htype == 'PhaseAmp':
			h = hp + 1j*hc
			ans = np.array([gwtools.phase(h), np.abs(h)])
		return ans
		
	#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	def out(self, hp, hc, htype, timesQ, freqsQ):
		
		# Cannot output both time and frequency samples with waveform
		if timesQ and freqsQ:
			raise Exception, "Must choose to output time samples, frequency samples, or neither."
		else:
			if timesQ:
				self.T = len(hp)
				x = self.time(self.T)
			if freqsQ:
				x = self.freq(self.F)
			
			if timesQ or freqsQ:
				ans = self.return_waveform(hp, hc, htype)
				dim = np.shape(ans)
				if len(dim) == 2:
					return np.array([x, ans[0], ans[1]])
				else:
					return np.array([x, ans])
			
			# If neither time nor frequency samples are requested then just return waveform
			if not timesQ and not freqsQ:
				return self.return_waveform(hp, hc, htype)
		
		pass


##############################################
class EOBNR(Setup):
	"""
	Class to generate EOBNR waveforms from the LIGO Algorithms Library (LAL)
	from non-spinning binary black hole coalescences. 
	
	Support included to produce waveforms in both the time and frequency domains.
	"""
	
	#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	def __init__(self):
		Setup.__init__(self)	
		self.switch_options = {
			'v2': self.v2,
			'v2modes': self.v2modes,
		}
		self.options = self.switch_options.keys()
		pass
	
	#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	def __call__(self, args, kwargs=None, eob_type='v2'):
		if kwargs == None:
			return self.switch_options[eob_type](*args)
		else:
			return self.switch_options[eob_type](*args, **kwargs)
	
	#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	def v2(self, m1, m2, incl=0., phiref=0., fmin=40., sample=8192., df=1./8, dist=1., \
			taperQ=False, htype='PhaseAmp', samplesQ=True, dom='TD'):
		"""
		Returns the (2,2) mode of an EOBNRv2 waveform generated
		by the LIGO Algorithms Library.
		
		Input
		-----
			m1		--- Mass of body 1 (in solar masses)
			m2		--- Mass of body 2 (in solar masses)
		
		Optional input
		--------------
			incl	--- inclination (default=0)
			phiref	--- reference orbital phase (default=0)
			fmin 	---	starting frequency (in Hz, default=40)
			sample	--- sample rate (in sec, default=8192)
			df		--- frequency bin size (in Hz, default=1./8)
			dist	---	distance to binary (in Mpc, default=1)
			taperQ	--- taper first few cycles? (default=False)
			htype	--- waveform components to output (default='PhaseAmp')
			timesQ	---	output time samples? (default=True)
			dom		--- set domain (default='TD')
		
		Output
		------
			Output depends on the htype option. 
		
		NOTE: If dom='FD' then the waveform is generated in TD, then FFT'd. 
		The frequency bin size df is determined by the length of the time-
		domain (TD) signal. Setting df=0 means the TD waveform will be padded 
		to the next power of 2. For df!=0, the TD waveform will be padded 
		further or a warning will be given if the waveform is so long that a 
		smaller df is required.			
		"""
		
		# Set parameters
		self.dt = 1./sample
		M1, M2 = m1*gwtools.Msuninkg, m2*gwtools.Msuninkg 
		Dist = dist*gwtools.Mpcinm 
		
		# Generate + and x waveform components
		hp, hc = lalsim.SimIMREOBNRv2DominantMode(phiref, self.dt, M1, M2, fmin, Dist, incl)
		
		# Taper start of waveform if requested
		if taperQ == True:
			self.taper(hp.data, hc.data)
		
		# Output the requested waveform component depending on specified domain
		if dom == 'TD':
			return self.out(hp.data.data, hc.data.data, htype, samplesQ, False)
		elif dom == 'FD':
			# FFT the + and x waveform components
			hpoff, hcoff = self.lal_fft(hp, hc, sample, df, fmin)
			return self.out(hpoff.data.data, hcoff.data.data, htype, False, samplesQ)
		else:
			raise Exception, "Set dom to be time-domain ('TD') or frequency-domain ('FD')."
		pass
	
	#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	def v2modes(self, m1, m2, incl=0., phiref=0., fmin=40., sample=8192., dist=1., \
			taperQ=False, htype='PhaseAmp', timesQ=True, dom='TD'):
		"""
		!!!UNFINISHED!!!
		"""
		
		# Set parameters
		self.dt = 1./sample
		M1, M2 = m1*gwtools.Msuninkg, m2*gwtools.Msuninkg
		Dist = dist*gwtools.Mpcinm
		
		approx = lalsim.EOBNRv2HM
		modes = [ [2,2], [2,1], [3,3], [4,4], [5,5] ]
		
		# Generate + and x waveform components
		# NOTE: LAL is only outputting the (5,5) mode...
		#h = lalsim.SimIMREOBNRv2AllModes(phiref, self.dt, M1, M2, fmin, Dist, incl)
		#h = lalsim.SimIMREOBNRv2Modes(phiref, self.dt, M1, M2, fmin, Dist)
		h = [] 
		for lm in modes:
			ll, mm = lm
			h.append(lalsim.SimInspiralChooseTDMode(phiref, self.dt, M1, M2, fmin, self.fref, \
			Dist, self.lambda1, self.lambda2, self.waveFlags, self.nonGRparams, 0, 7, \
			ll, mm, approx))
		self.h = h
		
		# Taper start of waveform if requested
		if taperQ:
			self.taper(hp.data, hc.data)
		
		return self.out(hp.data.data, hc.data.data, htype, samplesQ, False)
	

##############################################
class SEOBNR(Setup):
	"""
	Class to generate SEOBNR waveforms from the LIGO Algorithms Library (LAL)
	from spinning but non-precessing binary black hole coalescences. 
	
	Support included to produce waveforms in both the time and frequency domains.
	"""

	#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	def __init__(self):
		Setup.__init__(self)
		self.switch_options = {
			'v1': self.v1,
			'v2': self.v2,
		}
		self.options = self.switch_options.keys()
		pass
		
	#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	def __call__(self, args, kwargs=None, eob_type='v2'):
		if kwargs == None:
			return self.switch_options[eob_type](*args)
		else:
			return self.switch_options[eob_type](*args, **kwargs)
	
	#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	def v1(self, m1, m2, s1z, s2z, incl=0., phiref=0., fmin=40., sample=8192., \
			df=1./8, dist=1., taperQ=False, htype='PhaseAmp', samplesQ=True, dom='TD'):
		"""
		Returns the (2,2) mode of an SEOBNRv1 time-domain waveform generated
		by the LIGO Algorithms Library.
		
		Input
		-----
			m1		--- Mass of body 1 (in solar masses)
			m2		--- Mass of body 2 (in solar masses)
			s1z		--- Dimensionless spin of body 1 (<=1)
			s2z		--- Dimensionless spin of body 2 (<=1)
		
		Optional input
		--------------
			incl	--- inclination (default=0)
			phiref	--- reference orbital phase (default=0)
			fmin 	---	starting frequency (in Hz, default=40)
			sample	--- sample rate (in sec, default=8192)
			df		---	frequency bin size (in Hz, default=1./8)
			dist	---	distance to binary (in Mpc, default=1)
			taperQ	--- taper first few cycles? (default=False)
			htype	--- waveform components to output (default='PhaseAmp')
			timesQ	---	output time samples? (default=True)
		
		Output
		------
			Output depends on the htype option. 
		
		NOTE: If dom='FD' then the waveform is generated in TD, then FFT'd. 
		The frequency bin size df is determined by the length of the time-
		domain (TD) signal. Setting df=0 means the TD waveform will be padded 
		to the next power of 2. For df!=0, the TD waveform will be padded 
		further or a warning will be given if the waveform is so long that a 
		smaller df is required.			
		"""
		
		# Set parameters
		self.dt = 1./sample
		M1, M2 = m1*gwtools.Msuninkg, m2*gwtools.Msuninkg
		Dist = dist*gwtools.Mpcinm
		
		# Generate + and x waveform components
		hp, hc = lalsim.SimIMRSpinAlignedEOBWaveform(phiref, self.dt, M1, M2, \
			fmin, Dist, incl, s1z, s2z, 1)
		
		# Taper start of waveform if requested
		if taperQ:
			self.taper(hp.data, hc.data)
		
		# Output the requested waveform component depending on specified domain
		if dom == 'TD':
			return self.out(hp.data.data, hc.data.data, htype, samplesQ, False)
		elif dom == 'FD':
			# FFT the + and x waveform components
			hpoff, hcoff = self.lal_fft(hp, hc, sample, df, fmin)
			return self.out(hpoff.data.data, hcoff.data.data, htype, False, samplesQ)
		else:
			raise Exception, "Set dom to be time-domain ('TD') or frequency-domain ('FD')."
		pass
	
	#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	def v2(self, m1, m2, s1z, s2z, incl=0., phiref=0., fmin=40., sample=8192., \
			df=1./8, dist=1., taperQ=False, htype='PhaseAmp', samplesQ=True, dom='TD'):
		"""
		Returns the (2,2) mode of an SEOBNRv2 time-domain waveform generated
		by the LIGO Algorithms Library.
		
		Input
		-----
			m1		--- Mass of body 1 (in solar masses)
			m2		--- Mass of body 2 (in solar masses)
			s1z		--- Dimensionless spin of body 1 (<=1)
			s2z		--- Dimensionless spin of body 2 (<=1)
		
		Optional input
		--------------
			incl	--- inclination (default=0)
			phiref	--- reference orbital phase (default=0)
			fmin 	---	starting frequency (in Hz, default=40)
			sample	--- sample rate (in sec, default=8192)
			df		---	frequency bin size (in Hz, default=1./8)
			dist	---	distance to binary (in Mpc, default=1)
			taperQ	--- taper first few cycles? (default=False)
			htype	--- waveform components to output (default='PhaseAmp')
			timesQ	---	output time samples? (default=True)
		
		Output
		------
			Output depends on the htype option. 
		
		NOTE: If dom='FD' then the waveform is generated in TD, then FFT'd. 
		The frequency bin size df is determined by the length of the time-
		domain (TD) signal. Setting df=0 means the TD waveform will be padded 
		to the next power of 2. For df!=0, the TD waveform will be padded 
		further or a warning will be given if the waveform is so long that a 
		smaller df is required.			
		"""
		
		# Set parameters
		self.dt = 1./sample
		M1, M2 = m1*gwtools.Msuninkg, m2*gwtools.Msuninkg
		Dist = dist*gwtools.Mpcinm
		
		# Generate + and x waveform components
		hp, hc = lalsim.SimIMRSpinAlignedEOBWaveform(phiref, self.dt, M1, M2, \
			fmin, Dist, incl, s1z, s2z, 2)
		
		# Taper start of waveform if requested
		if taperQ:
			self.taper(hp.data, hc.data)
		
		# Output the requested waveform component depending on specified domain
		if dom == 'TD':
			return self.out(hp.data.data, hc.data.data, htype, samplesQ, False)
		elif dom == 'FD':
			# FFT the + and x waveform components
			hpoff, hcoff = self.lal_fft(hp, hc, sample, df, fmin)
			return self.out(hpoff.data.data, hcoff.data.data, htype, False, samplesQ)
		else:
			raise Exception, "Set dom to be time-domain ('TD') or frequency-domain ('FD')."
		pass
	

##############################################
class Phenom(Setup):
	
	#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	def __init__(self):
		Setup.__init__(self)	
		self.switch_options = {
			'A': self.A,
			'B': self.B,
			'C': self.C,
			#'P': self.P,
		}
		self.options = self.switch_options.keys()
		pass
	
	#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	def __call__(self, args, kwargs=None, phenom_type='C'):
		if kwargs == None:
			return self.switch_options[phenom_type](*args)
		else:
			return self.switch_options[phenom_type](*args, **kwargs)
	
	#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	def A(self, m1, m2, incl=0., phiref=0., fmin=40., sample=8192., df=1./8, dist=1., \
			taperQ=False, htype='PhaseAmp', samplesQ=True, dom='FD'):
	
		"""
		Returns IMRPhenomA TD waveform in a REAL8TimeSeries object
		
		Mandatory params: m1 and m2
		
		Optional params: inclination (incl), reference orbital phase (phiref)
					starting freq. (fmin), sample rate (sample)
					sky position (thetaS, phiS), polarization angle (psi)
					distance (dist)
					If taper=True, will taper first few cycles of TD waveform
		
		Units: m1, m2 in solarmasses, dist in Mpc 
				time/freq in s/Hz, angles in radians
		"""
		
		# Set parameters
		self.dt = 1./sample
		M1, M2 = m1*gwtools.Msuninkg, m2*gwtools.Msuninkg
		Dist = dist*gwtools.Mpcinm
		
		if dom == 'TD':
			# Generate + and x waveform components 
			hp, hc = lalsim.SimIMRPhenomAGenerateTD(phiref, self.dt, M1, M2, fmin, self.fref, Dist, incl)
	
			# Taper start of waveform if requested
			if taperQ:
				self.taper(hp.data, hc.data)
			
			# Output the requested waveform component
			return self.out(hp.data.data, hc.data.data, htype, samplesQ, False)
		
		elif dom == 'FD':
			# Generate + and x waveform components 
			fNyq = sample/2.
			hoff = lalsim.SimIMRPhenomAGenerateFD(phiref, df, M1, M2, fmin, fNyq, Dist)
			self.F = len(hoff.data.data)
			self.df = df
			self.fmin = fmin
		
			# Output the requested waveform component
			return self.out(np.real(hoff.data.data), np.imag(hoff.data.data), htype, False, samplesQ)		
		
		else:
			raise Exception, "Set dom to be time-domain ('TD') or frequency-domain ('FD')."
		
		pass
		
	#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	def B(self, m1, m2, chi, incl=0., phiref=0., fmin=40., sample=8192., df=1./8, dist=1., \
			taperQ=False, htype='PhaseAmp', samplesQ=True, dom='FD'):
	
		"""
		Returns IMRPhenomA TD waveform in a REAL8TimeSeries object
		
		Mandatory params: m1 and m2
		
		Optional params: inclination (incl), reference orbital phase (phiref)
					starting freq. (fmin), sample rate (sample)
					sky position (thetaS, phiS), polarization angle (psi)
					distance (dist)
					If taper=True, will taper first few cycles of TD waveform
		
		Units: m1, m2 in solarmasses, dist in Mpc 
				time/freq in s/Hz, angles in radians
		"""
		
		# Set parameters
		self.dt = 1./sample
		M1, M2 = m1*gwtools.Msuninkg, m2*gwtools.Msuninkg
		Dist = dist*gwtools.Mpcinm
		
		if dom == 'TD':
			# Generate + and x waveform components 
			hp, hc = lalsim.SimIMRPhenomBGenerateTD(phiref, self.dt, M1, M2, chi, fmin, self.fref, Dist, incl)
	
			# Taper start of waveform if requested
			if taperQ:
				self.taper(hp.data, hc.data)
			
			# Output the requested waveform component
			return self.out(hp.data.data, hc.data.data, htype, samplesQ, False)
		
		elif dom == 'FD':
			# Generate + and x waveform components 
			fNyq = sample/2.
			hoff = lalsim.SimIMRPhenomBGenerateFD(phiref, df, M1, M2, chi, fmin, fNyq, Dist)
			self.F = len(hoff.data.data)
			self.df = df
			self.fmin = fmin
		
			# Output the requested waveform component
			return self.out(np.real(hoff.data.data), np.imag(hoff.data.data), htype, False, samplesQ)		
		
		else:
			raise Exception, "Set dom to be time-domain ('TD') or frequency-domain ('FD')."
		
		pass
		
	#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	def C(self, m1, m2, chi, incl=0., phiref=0., fmin=40., sample=8192., df=1./8, dist=1., \
			taperQ=False, htype='PhaseAmp', samplesQ=True, dom='FD'):
	
		"""
		Returns IMRPhenomA TD waveform in a REAL8TimeSeries object
		
		Mandatory params: m1 and m2
		
		Optional params: inclination (incl), reference orbital phase (phiref)
					starting freq. (fmin), sample rate (sample)
					sky position (thetaS, phiS), polarization angle (psi)
					distance (dist)
					If taper=True, will taper first few cycles of TD waveform
		
		Units: m1, m2 in solarmasses, dist in Mpc 
				time/freq in s/Hz, angles in radians
		"""
		
		# Set parameters
		self.dt = 1./sample
		M1, M2 = m1*gwtools.Msuninkg, m2*gwtools.Msuninkg
		Dist = dist*gwtools.Mpcinm
		
		if dom == 'TD':
			# Generate + and x waveform components 
			hp, hc = lalsim.SimIMRPhenomCGenerateTD(phiref, self.dt, M1, M2, chi, fmin, self.fref, Dist, incl)
	
			# Taper start of waveform if requested
			if taperQ:
				self.taper(hp.data, hc.data)
			
			# Output the requested waveform component
			return self.out(hp.data.data, hc.data.data, htype, samplesQ, False)
		
		elif dom == 'FD':
			# Generate + and x waveform components 
			fNyq = sample/2.
			hoff = lalsim.SimIMRPhenomCGenerateFD(phiref, df, M1, M2, chi, fmin, fNyq, Dist)
			self.F = len(hoff.data.data)
			self.df = df
			self.fmin = fmin
		
			# Output the requested waveform component
			return self.out(np.real(hoff.data.data), np.imag(hoff.data.data), htype, False, samplesQ)		
		
		else:
			raise Exception, "Set dom to be time-domain ('TD') or frequency-domain ('FD')."
		
		pass

##############################################
class Taylor(Setup):

	#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	def __init__(self):
		Setup.__init__(self)	
		self.switch_options = {
			'F2restricted': self.F2restricted,
			'T4restricted': self.T4restricted,
		}
		self.options = self.switch_options.keys()
		pass
	
	#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	def __call__(self, args, kwargs=None, taylort4_type='TD'):
		if kwargs == None:
			return self.switch_options[eob_type](*args)
		else:
			return self.switch_options[eob_type](*args, **kwargs)
		
	#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	def F2restricted(self, m1, m2, incl=0., phiref=0., fmin=40., sample=8192., df=1./8, dist=1., \
			htype='PhaseAmp', samplesQ=True):
		"""INCOMPLETE"""
		
		# Set parameters
		self.dt = 1./sample
		M1, M2 = m1*gwtools.Msuninkg, m2*gwtools.Msuninkg
		Dist = dist*gwtools.Mpcinm
		s1z, s2z = 0., 0.
		quadparam1, quadparam2 = 1, 1
		lambda1, lambda2 = 0., 0.
		fNyq = sample/2.
		spinO = 0
		tideO = 0
		phaseO = -1
		ampO = 0
		
		# Generate + and x waveform components
		hoff = lalsim.SimInspiralTaylorF2(phiref, df, m1, m2, s1z, s2z, fmin, fNyq, \
			self.fref, Dist, quadparam1, quadparam2, lambda1, lambda2, -1, 0, -1, 0)
		
		self.F = len(hoff.data.data)
		self.df = df
		self.fmin = fmin
		
		return self.out(np.real(hoff.data.data), np.imag(hoff.data.data), htype, False, samplesQ)
	
	#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	def T4restricted(self, m1, m2, incl=0., phiref=0., fmin=40., sample=8192., df=1./8, dist=1., \
			taperQ=False, htype='PhaseAmp', samplesQ=True, dom='TD'):
		"""
		Returns TaylorT4 TD waveform in a REAL8TimeSeries object
		
		Mandatory params: m1 and m2
		
		Optional params: phase PN order (phaseO), amplitude PN order (ampO)
					inclination (incl), reference orbital phase (phiref)
					starting freq. (fmin), sample rate (sample)
					sky position (thetaS, phiS), polarization angle (psi)
					distance (dist)
					If taper=True, will taper first few cycles of TD waveform
		
		Units: m1, m2 in solarmasses, dist in Mpc 
				time/freq in s/Hz, angles in radians
		
		NOTE: phaseO, ampO are PN order counting by halves (i.e. 7=3.5PN)
		"""
		
		# Set parameters
		self.dt = 1./sample
		M1, M2 = m1*gwtools.Msuninkg, m2*gwtools.Msuninkg
		Dist = dist*gwtools.Mpcinm
		
		# Generate + and x waveform components
		hp, hc = lalsim.SimInspiralTaylorT4PNRestricted(phiref, self.dt, M1, M2, \
			fmin, self.fref, Dist, incl, self.lambda1, self.lambda2, 0, 7)
		
		# Taper start of waveform if requested
		if taperQ:
			self.taper(hp.data, hc.data)
		
		# Output the requested waveform component depending on specified domain
		if dom == 'TD':
			return self.out(hp.data.data, hc.data.data, htype, samplesQ, False)
		elif dom == 'FD':
			# FFT the + and x waveform components
			hpoff, hcoff = self.lal_fft(hp, hc, sample, df, fmin)
			return self.out(hpoff.data.data, hcoff.data.data, htype, False, samplesQ)
		else:
			raise Exception, "Set dom to be time-domain ('TD') or frequency-domain ('FD')."
		pass

