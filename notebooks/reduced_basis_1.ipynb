{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This notebook shows an example of how to use `rompy` and its gravitational wave interface `romgw` for building a reduced basis representation for gravitational waveforms produced by compact binary inspirals.\n",
    "\n",
    "Note: Until a proper installation script is made, it is useful to have `rompy` and `romgw` in the same directory and set your PYTHONPATH to point to that directory in .bash_profile."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import numpy as np, matplotlib.pyplot as plt\n",
    "%matplotlib inline\n",
    "import romgw as rom"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's define our parameters. We'll consider non-spinning binaries with fixed total masses and mass ratios in [1,10]."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "Msun = rom.Msuninsec   # Define solar mass in seconds\n",
    "Mtot = 6.*Msun         # Fixed total mass\n",
    "qmin = 1.\n",
    "qmax = 10.\n",
    "Mcmin = rom.Mq_to_Mc(Mtot, qmax)   # Minimum chirp mass\n",
    "Mcmax = rom.Mq_to_Mc(Mtot, qmin)   # Maximum chirp mass"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The frequency range will be start at 40Hz, just for the sake of demonstration, and go up to the ISCO frequency for the lowest total mass binary."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "fmin = 40.\n",
    "fmax = rom.fgwisco(Mtot)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we need to define an inner product so we can compute norms and overlaps of waveforms. We'll use the complex inner product with the Chebyshev Gauss-Lobatto quadrature rule for 10000 nodes to numerically evaluate the integrals."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "inner = rom.InnerProduct([fmin, fmax, 10000], 'chebyshev-gauss-lobatto')\n",
    "inner_type = 'complex'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For simplicity, we'll consider 0PN TaylorF2 SPA waveforms and make a template."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "template = rom.pnspa.TaylorF2(inner.nodes)\n",
    "pn_type = '0'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we need a training set of 0PN waveforms. We'll first create a training space of the chirp masses. It's useful to build this training space with forehand knowledge of how the density of waveforms varies. Of course, we may not know this *a priori* so let's just create a uniformly spaced training set of chirp masses. Later on, we'll make a non-uniformly spaced training set with input from the phase structure of the 0PN waveform."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "tr = rom.training.Uniform()\n",
    "TMc = tr([Mcmin, Mcmax, 600], '1d')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`TMc` is the training set of chirp masses. The training set of normalized 0PN waveforms is thus given by a simple list comprehension:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "Th = np.array( [inner.normalize(template([mc], pn_type), inner_type) for mc in TMc] )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that `pn_type` denotes the PN order and `inner_type` denotes the type of inner product. If in doubt about the available options just type:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print inner.options\n",
    "print template.options"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here are the first and last waveforms in that training space: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print Th[0]\n",
    "print Th[-1]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To build a reduced basis we use a greedy algorithm. As this particular problem is fairly standard (insofar as reduced order modeling goes), we can use the StandardRB class to build the reduced basis. First, we create an instance of the class, which is found in the algorithm module of `rompy`. As we don't know how many basis waveforms will be selected we'll place a maximum of 500 elements."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "Nbasis = 500\n",
    "rb = rom.algorithms.ReducedBasis(Nbasis, inner, inner_type)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Back on course with the example. Next, we need to seed the greedy algorithm using the `seed` method. We'll arbitrarily choose the seed waveform to be the first one appearing in the training space. (You can experiment and see how the number of basis elements changes as you change the seed waveform.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "rb.seed(0, Th)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Seeding the algorithm has created some arrays behind the scenes that will be populated as the greedy algorithm is iterated. In particular, `seed` has created four arrays (whose dimensions are printed below):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print rb.errors.shape  # Max projection errors from each step\n",
    "print rb.indices.shape # Array indices of training set that maximize projection error from each step\n",
    "print rb.basis.shape   # Orthonormal basis functions from each step\n",
    "print rb.alpha.shape   # Matrix of inner products between basis functions and training space waveforms"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As an important side note, if we find that these arrays are not large enough because we have underestimated the number of basis functions that will be found we can easily extend these using the `rom.malloc_more` function. For example, let's say we want to add 100 more entries to just `rb.errors` (in practice, we would add this to all the arrays). Then, we make a call to `rom.malloc_more` as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "rb.errors = rom.malloc_more(rb.errors, 100)\n",
    "rb.errors.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Fine, but now we need to subtract those 100 elements we added to continue on with our example... To do that we'll make a call to `rom.trim` where the number specified is the number of elements/rows we want in the final array."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "rb.errors = rom.trim(rb.errors, Nbasis)\n",
    "rb.errors.shape   # Make sure the array dimensions are what we want"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we're ready to run the greedy algorithm, which is performed by the for-loop below. We'll take the projection error tolerance for the reduced basis to be 1e-12 so that any waveform in the training space will be represented by its projection onto the reduced basis with an error less than 1e-12."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "tol = 1e-12\n",
    "\n",
    "for ctr in range(Nbasis-1):\n",
    "\n",
    "    if rb.errors[ctr] < tol:\n",
    "        break\n",
    "\n",
    "    errs = rb.proj_errors_from_alpha(rb.alpha[:ctr+1])    # Compute greedy errors\n",
    "    rb.iter(ctr, errs, Th)                             \n",
    "    print ctr+1, \"   \", rb.errors[ctr]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We thus find that 342 basis elements are needed to span the space of 0PN waveforms in our range of chirp mass and frequency. Note that the size of our arrays are still `Nbasis` long..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print rb.errors.shape\n",
    "print rb.alpha.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "but we only have 342 waveforms. We'll use the special `trim` function that's in StandardRB to take care of this in one fell swoop instead of applying `rom.trim` separately to each array we want to trim:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "rb.trim(ctr)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now the shapes reflect the size of the basis:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print rb.errors.shape\n",
    "print rb.alpha.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's have a look at the maximum reduced basis projection errors over the training set, which has a very flat plateau and fast exponential decay. As more waveforms are added to the reduced basis, the maximum projection error of the training set waveforms onto the current basis decreases until the preset tolerance is reached."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "plt.semilogy(range(1, ctr+1), rb.errors, 'k-')\n",
    "plt.xlabel('Basis size')\n",
    "plt.ylabel('Maximum projection error')\n",
    "plt.ylim(1e-12, 2);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The distribution of chirp masses selected by the greedy algorithm is:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "plt.hist(TMc[rb.indices]/Msun, 20, color='k', alpha=0.33)\n",
    "plt.xlabel('Chirp mass [$M_\\\\odot$]');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "How accurately does the reduced basis span the *continuum* space of 0PN waveforms in the given frequency and chirp mass ranges? To answer this, it's useful to randomly select many waveforms, project them onto the basis, and compute their mismatch with the actual waveform. We won't minimize the mismatch over relative phase and time because we want to assess the quality of the reduced basis representation itself, not how well we can minimize the error.\n",
    "\n",
    "\n",
    "\n",
    "To do this, we'll need to create a randomly sampled set of waveforms, which we can conveniently achieve with the `training.Random` class"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "Nrand = 1000\n",
    "trand = rom.training.Random()\n",
    "TMc_rand = trand([Mcmin, Mcmax, Nrand], 'uniform')\n",
    "Th_rand = np.array( [inner.normalize(template([mc], pn_type), inner_type) for mc in TMc_rand] )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now compute the mistmach due to projecting onto the basis by calling `rb.proj_error_from_basis`..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "rb_errs = [rb.proj_mismatch_from_basis(rb.basis, hh) for hh in Th_rand]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "and plot them as a function of the randomly selected chirp masses:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "plt.semilogy(TMc_rand/Msun, rb_errs, 'k.')\n",
    "plt.xlabel('Chirp mass [$M_\\\\odot$]');\n",
    "plt.ylabel('Projection errors');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Unfortunately, for binaries with lower chirp masses this reduced basis does not accurately represent those waveforms. On the other hand, binaries with chirp masses larger than about 1.6 $M_\\odot$ are represented at the level of numerical round-off error. This example is interesting because we will see next that by informing the training set of some basic and readily accessible information about the 0PN waveform phase will resolve this issue.\n",
    "\n",
    "\n",
    "\n",
    "Generating a training set of chirp masses that are distributed in a manner at least crudely consistent with what we expect, namely, that the density of waveforms for a given chirp mass bin is higher for smaller chirp masses, will improve the resulting reduced basis. In other words, a better-informed training set makes it easier for the greedy algorithm to learn about the structure of the training space."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "tr2 = rom.training.NonUniform()   # Create an instance of the training.NonUniform class"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we need to define a function that will distribute the chirp masses accordingly. We only require that this function maps $[0,1]$ to itself."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def func(x):\n",
    "    return x**(5./3.)   # Scaling of 0PN phase at low mass ratio with fixed total mass"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we make the non-uniformly spaced training set of chirp masses and the training space of corresponding 0PN waveforms. Note that we're going to use the same number of elements in the training set as before, just distributed differently."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "TMc2 = tr2(func, [Mcmin, Mcmax, len(TMc)], '1d')\n",
    "Th2 = np.array( [inner.normalize(template([mc], pn_type), inner_type) for mc in TMc2] )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Compare this distribution of chirp masses to what we originally used earlier, which was uniformly sampled."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "plt.hist(TMc/Msun, 15, color='k', alpha=0.33, label='Uniform')\n",
    "plt.hist(TMc2/Msun, 15, color='b', alpha=0.5, label='$x^{5/3}$')\n",
    "plt.xlabel('Chirp mass')\n",
    "plt.ylabel('Number')\n",
    "plt.legend(loc='upper right');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, rerun the greedy algorithm with this new training set. Instead of rewriting the for-loop above we can use the `rb.make` function to separately make the reduced basis. However, to keep our original basis separate from the one we're about to make it is helpful to create a new instance of the StandardRB class:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "rb2 = rom.algorithms.ReducedBasis(Nbasis, inner, inner_type)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Make the reduced basis. Use the `make` method for convenience instead of retyping the greedy algorithm each time."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "rb2.make(0, Th2, tol, verbose=False)  # Set `verbose` key argument to False to prevent printing to screen"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The number of basis functions is about the same as before. But because we changed the set of waveforms used to train the greedy algorithm then we won't get exactly the same number of basis functions as we did before."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "rb2.size"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Alternatively, we could have thrown a lot more points into the training space for the uniformly spaced example we started out with. But that may drain our computing and memory resources fairly quickly, especially for waveforms that depend on several parameters like spins.\n",
    "\n",
    "Let's check how well this new basis does by repeating the earlier Monte Carlo projection errors study using the same random sample of parameters."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "rb2_errs = [rb2.proj_mismatch_from_basis(rb2.basis, hh) for hh in Th_rand]\n",
    "\n",
    "plt.semilogy(TMc_rand/Msun, rb2_errs, 'k.')\n",
    "plt.xlabel('Chirp mass [$M_\\\\odot$]');\n",
    "plt.ylabel('Projection errors');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This newer basis is highly accurate despite there being about the same number of waveforms selected by the greedy algorithm. Even the distribution of selected parameters is noticably different."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "plt.hist(TMc[rb.indices]/Msun, 15, color='k', alpha=0.33)\n",
    "plt.hist(TMc[rb2.indices]/Msun, 15, color='r', alpha=0.33)\n",
    "plt.xlabel('Chirp mass [$M_\\\\odot$]');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "An interesting exercise is to use the selected parameters from the first output of the greedy algorithm to build a distribution to draw chirp masses from for a second trainining space and rerun the greedy algorithm on the latter. Will the distribution of selected chirp masses be  uniform?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "To summarize, we used `rompy` and its gravitational wave interface `romgw` to build an accurate reduced basis representation to compress the waveform space in the chirp mass. There are more functions accessible in `rompy` and more waveform families available in `romgw` than shown here, though the latter is still in development and requires a local installation of the LIGO Algorithms Library (LAL). \n",
    "\n",
    "More complicated reduced order models may require using greedy algorithms built from other integration measures (e.g., the L-infinity norm instead of the L2 norm here). However, `rompy` is equipped with low-level functions to handle many application-specific requirements for building greedy algorithms."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
