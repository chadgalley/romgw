{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This notebook shows an example of how to use `rompy` and its gravitational wave interface `romgw` for building an empirical interpolant for gravitational waveforms produced by compact binary inspirals.\n",
    "\n",
    "Note: Until a proper installation script is made, it is useful to have `rompy` and `romgw` in the same directory and set your PYTHONPATH to point to that directory in .bash_profile."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import numpy as np, matplotlib.pyplot as plt\n",
    "import romgw as rom"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's define our parameters and setup as in the `reduced_basis_1.ipynb` notebook. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "Msun = rom.Msuninsec   # Define solar mass in seconds\n",
    "Mtot = 6.*Msun         # Fixed total mass\n",
    "qmin = 1.\n",
    "qmax = 10.\n",
    "Mcmin = rom.Mq_to_Mc(Mtot, qmax)   # Minimum chirp mass\n",
    "Mcmax = rom.Mq_to_Mc(Mtot, qmin)   # Maximum chirp mass\n",
    "\n",
    "fmin = 40.\n",
    "fmax = rom.fgwisco(Mtot)\n",
    "\n",
    "inner = rom.InnerProduct([fmin, fmax, 10000], 'chebyshev-gauss-lobatto')\n",
    "inner_type = 'complex'\n",
    "\n",
    "template = rom.pnspa.TaylorF2(inner.nodes)\n",
    "pn_type = '0'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Build the reduced basis for the second training set of 0PN waveforms considered in the `reduced_basis_1.ipynb` notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Generate the training set of parameters and waveforms\n",
    "tr = rom.training.NonUniform()\n",
    "\n",
    "def func(x):\n",
    "    return x**(5./3.)   # Scaling of 0PN phase at low mass ratio with fixed total mass\n",
    "\n",
    "TMc = tr(func, [Mcmin, Mcmax, 600], '1d')\n",
    "Th = np.array( [inner.normalize(template([mc], pn_type), inner_type) for mc in TMc] )\n",
    "\n",
    "\n",
    "# Build the reduced basis\n",
    "Nbasis = 400\n",
    "rb = rom.algorithms.StandardRB(Nbasis, len(TMc), len(inner.nodes), inner, inner_type)\n",
    "tol = 1e-12\n",
    "rb.make(0, Th, tol, verbose=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The reduced basis representation of the waveform space provides a compression in chirp mass since only 357 wavefunctions are needed to represent the entire space on this training interval. This was shown in the `reduced_basis_1.ipynb` notebook.\n",
    "\n",
    "Next, let's construct a dual compression in frequency by building the empirical interpolant. All we need to do is instantiate the StandardEIM class (EIM for \"Empirical Interpolation Method\") and run `make`. The function `make` only requires the reduced basis as input. This function generates the interpolation matrix that is called \"B\" in the surrogate model paper [see Field, Galley, Hesthaven, Kaye, and Tiglio, ${\\it Phys.~Rev.~X} ~ {\\bf 4}$, 031006 (2014)]. The size of this matrix is simply the number of reduced basis functions (357) by the number of frequency (or time, if in the time domain) samples (10000)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "eim = rom.algorithms.StandardEIM(Nbasis, len(inner.nodes))\n",
    "eim.make(rb.basis, verbose=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The number of frequency nodes for the empirical interpolant is, by construction, the same number of reduced basis waveforms:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print rb.size, eim.size"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Any waveform can be interpolated with the empirical interpolant. For example, if we have a random set of waveforms..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "Nrand = 1000\n",
    "trand = rom.training.Random()\n",
    "TMc_rand = trand([Mcmin, Mcmax, Nrand], 'uniform')\n",
    "Th_rand = np.array( [inner.normalize(template([mc], pn_type), inner_type) for mc in TMc_rand] )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "...then the 13th one, for example, has a frequency series that is accurately represented by the empirical interpolant:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print eim.interpolate(Th_rand[12])\n",
    "print Th_rand[12]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's see how well the empirical interpolant represents randomly selected waveforms. The reduced basis projection errors are also plotted (black dots) for comparison."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "rb_errs, eim_errs = [], []\n",
    "for ii, hh in enumerate(Th_rand):\n",
    "    rb_errs.append( rb.proj_mismatch_from_basis(rb.basis, hh) )\n",
    "    eim_errs.append( inner.norm(eim.interpolate(hh)-hh, inner_type)**2 )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "plt.semilogy(TMc_rand/Msun, eim_errs, 'r.', label='EIM $L_2$ errors')\n",
    "plt.semilogy(TMc_rand/Msun, rb_errs, 'k.', label='RB $L_2$ errors')\n",
    "plt.xlabel('Chirp mass [$M_\\\\odot$]');\n",
    "plt.ylabel('Interpolation errors');\n",
    "plt.legend(loc='lower center');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The maximum emperical interpolation representation error and the maximum reduced basis representation error for these randomly selected waveforms are:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print max(eim_errs)\n",
    "print max(rb_errs)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, here's a plot showing the distribution of frequency nodes for the empirical interpolant (and later for reduced-order quadratures) as a function of the iteration step (or, equivalently, the size of the reduced basis):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "plt.plot(inner.nodes[eim.indices], 'kx')\n",
    "plt.xlabel('Step');\n",
    "plt.ylabel('Frequency [Hz]');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "To summarize, we have an accurate reduced basis representation that compresses the waveform space in parameter (i.e., chirp mass) and an emperical interpolant that compresses the reduced basis representation in frequency. Building a reduced basis and an empirical interpolant are the first three steps of generating a surrogate model."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
