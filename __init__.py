# --- __init__.py ---

__copyright__ = "Copyright (C) 2013 Chad R. Galley"
__email__ = "crgalley@gmail.com, crgalley@tapir.caltech.edu"
__author__ = "Chad Galley"


import warnings as _warnings

try:
  from gwtools import *
  #import gwtools  # TODO: to prevent namespace collisions
except:
  raise Exception, "Cannot import gwtools."

try:
  from rompy import *
except:
  raise Exception, "Cannot import ROMpy."

# Import detector sensitivities and overlap integration rules
from psd import *
from overlap import *

# Import some waveform generation modules
import pnspa

#import spec  # Probably obsolete
try:
  import lalsimgw  # Can be significantly improved
except:
  _warnings.warn("Cannot import lalsimgw module.")
