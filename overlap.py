import numpy as np
from rompy import integrals


###########################################
# Classes for computing overlap integrals #
#       of gravitational waveforms        #
###########################################

class Overlap:
  
  def __init__(self, interval=None, num=None, rate=None, incr=None, rule='trapezoidal', nodes=None, weights=None):
    if nodes is None and weights is None:
      self.nodes, self.weights, self.rule = integrals._nodes_weights(interval, num, rate, incr, rule)
    else:
      self.nodes, self.weights = nodes, weights
  
  def integral(self, f):
    """Integral of a function"""
    return np.dot(self.weights, f)
  
  def dot(self, f, g):
    """Dot product of two functions"""
    dim = np.shape(f)
    if len(dim) == 1:
      return 4.*np.dot(self.weights, np.real(np.conj(f)*g))
    else:
      f_conj = np.conj(f)
      return 4.*np.dot(self.weights, np.sum( np.real(f_conj[ii]*g[ii]) for ii in range(dim[0])))
  
  def norm(self, f):
    """Norm of function"""
    return np.sqrt(np.real(self.dot(f, f)))
  
  def normalize(self, f):
    """Normalize a function"""
    return f / self.norm(f)
  
  def match(self, f, g):
    """Match integral"""
    f_normed = self.normalize(f)
    g_normed = self.normalize(g)
    return np.real(self.dot(f_normed, g_normed))
  
  def mismatch(self, f, g):
    """Mismatch integral (1-match)"""
    return 1.-self.match(f, g)


class Integration(integrals.Integration, Overlap):
  """Integrals for computing inner products and norms of functions"""
  
  def __init__(self, interval=None, num=None, rate=None, incr=None, rule='trapezoidal', nodes=None, weights=None):
    integrals.Integration.__init__(self, interval=interval, num=num, rate=rate, incr=incr, rule=rule, nodes=nodes, weights=weights)
    
    self.overlap = Overlap(nodes=self.nodes, weights=self.weights)
    
    # Add overlap to integration options in RomPy
    self._dict['overlap'] = self.overlap
    self.options = self._dict.keys()

