# --- spec.py ---

"""
	Loading SpEC waveforms from hdf5 file format
"""


import h5py, numpy as np, subprocess
import gwtools


#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
def file_len(fname):
	"""Get total number of lines in file."""
	p = subprocess.Popen(['wc', '-l', fname], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	result, err = p.communicate()
	if p.returncode != 0:
		raise IOError(err)
	return int(result.strip().split()[0])

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
def read_text(path, delim=' '):
	file = open(path)
	#buffer = np.zeros((file_len(path)), dtype='double')
	buffer = []
	ctr = 0
	for ii, line in enumerate(file):
		if line[0] != '#':
			#buffer[ctr] = [float(s) for s in line.strip().split(delim)]
			buffer.append([float(s) for s in line.strip().split(delim)])
			ctr += 1
	file.close()
	return buffer

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
def dat_to_hdf5(datpath, h5path=None, lmax=8, lmin=2):
	"""Convert SpEC text data format to hdf5 format."""
	
	# Load in waveform data from text file
	datfile = open(datpath)
	modes = lmarray(lmax, lmin)
	buffer = np.zeros((file_len(datpath), 1+len(2*modes)), dtype='double')
	ctr = 0
	for ii, line in enumerate(datfile):
		if line[0] != '#':
			buffer[ctr] = [float(s) for s in line.strip().split('\t')]
			ctr += 1
	datfile.close()
	
	# Write time, phases, and amplitudes to hdf5 format
	if h5path == None:
		h5path = datpath
	h5file = h5py.File(h5path, 'w')
	h5file.create_dataset('t-r*', data=buffer[:,0], dtype='double', compression='gzip')
	phases = h5file.create_group('Phase')
	amps = h5file.create_group('Amp')
	ctr2 = 1
	for ii, mm in enumerate(modes):
		amps.create_dataset('['+str(mm[0])+','+str(mm[1])+']', data=buffer[:, ctr2], dtype='double', compression='gzip')
		phases.create_dataset('['+str(mm[0])+','+str(mm[1])+']', data=buffer[:, ctr2+1], dtype='double', compression='gzip')
		ctr2 += 2
	h5file.close()
	
	pass

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
def hdf5_to_hdf5_old(readpath, writepath=None):
	readfile = h5py.File(readpath, 'r')
	
	modes = []
	for kk in readfile.keys():
		if kk[:3] == 'Amp':
			buffer = kk.strip().split(',')
			modes.append([int(buffer[0][-1]), int(buffer[1][:-2])])

	if writepath == None:
		writepath = readpath
	writefile = h5py.File(writepath, 'w')
	writefile.create_dataset('t-r*', data=readfile['(t-r*)/M'][:])
	phases = writefile.create_group('Phase')
	amps = writefile.create_group('Amp')
	for ii, mm in enumerate(modes):
		l, m = mm
		amps.create_dataset('['+str(l)+','+str(m)+']', data=readfile['Amp{rhOverM('+str(l)+','+str(m)+')}'][:], dtype='double', compression='gzip')
		phases.create_dataset('['+str(l)+','+str(m)+']', data=readfile['Arg{rhOverM('+str(l)+','+str(m)+')}'][:], dtype='double', compression='gzip')
	writefile.close()
	readfile.close()
	
	pass

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
def h5_to_h5(readpath, writepath=None, subdir=''):
	readfile = h5py.File(readpath, 'r')
	
	if subdir == '':
		keys = readfile.keys()
	else:
		keys = readfile.get(subdir).keys()

	modes = []
	for kk in keys:
		if kk[0] == 'Y':
			buffer = kk.strip().replace('_l', '_').replace('_m', '_').replace('.dat', '').split('_')
			modes.append([int(buffer[1]), int(buffer[2])])

	if writepath == None:
		writepath = readpath
	writefile = h5py.File(writepath, 'w')
	t = np.transpose(readfile[subdir+'Y_l2_m2.dat'][:])[0]
	writefile.create_dataset('t-r*', data=t)
	amps = writefile.create_group('Amp')
	phases = writefile.create_group('Phase')
	hplus = writefile.create_group('hplus')
	hcross = writefile.create_group('hcross')
	for ii, mm in enumerate(modes):
		l, m = mm
		t, hp, hc = np.transpose(readfile[subdir+'Y_l'+str(l)+'_m'+str(m)+'.dat'][:])
		h = hp+1j*hc
		amp = np.abs(h)
		phi = gwmisc.phase(h)
		amps.create_dataset('['+str(l)+','+str(m)+']', data=amp, dtype='double', compression='gzip')
		phases.create_dataset('['+str(l)+','+str(m)+']', data=phi, dtype='double', compression='gzip')
		hplus.create_dataset('['+str(l)+','+str(m)+']', data=hp, dtype='double', compression='gzip')
		hcross.create_dataset('['+str(l)+','+str(m)+']', data=hc, dtype='double', compression='gzip')
	writefile.close()
	readfile.close()
	
	pass

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
def ccedat_to_h5(readpath, writepath, subdir='', lmax=8, lmin=2):
	
	# Load in waveform data from text file
	modes = lmarray(lmax, lmin=lmin)
	filenames = ['h_from_rPsi4_l'+str(lm[0])+'m'+str(lm[1])+'.dat' for lm in modes]
	
	# Open h5 file for writing
	out = h5py.File(writepath+'rhOverM_Asymptotic_GeometricUnits.h5', 'w')
	hplus = out.create_group('hplus')
	hcross = out.create_group('hcross')
	amps = out.create_group('Amp')
	phases = out.create_group('Phase')

	# Open dat files for converting to h5
	for jj, ff in enumerate(filenames):
		ctr = 0
		lm = modes[jj]
		print lm
		buffer = np.zeros((file_len(readpath+ff), 3), dtype='double')
		datfile = open(readpath+ff)
		for ii, line in enumerate(datfile):
			if line[0] != '#':
				buffer[ctr] = [float(s) for s in line.strip().split(' ')]
				ctr += 1
		datfile.close()

		# Write data to h5
		t = buffer[:,0]
		hp = buffer[:,1]
		hx = buffer[:,2]
		h = hp+1j*hx
		amps.create_dataset('['+str(lm[0])+','+str(lm[1])+']', data=np.abs(h), compression='gzip')
		phases.create_dataset('['+str(lm[0])+','+str(lm[1])+']', data=misc.phase(h), compression='gzip')
		hplus.create_dataset('['+str(lm[0])+','+str(lm[1])+']', data=hp, compression='gzip')
		hcross.create_dataset('['+str(lm[0])+','+str(lm[1])+']', data=hx, compression='gzip')
			
	out.create_dataset('t-r*', data=t, dtype='double', compression='gzip')
	out.close()
	pass
	
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
def get_runinfo(path):
	return np.transpose(read_text(path, delim='\t'))

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
def filenames(path):
	buffer = read_text(path, delim='\t')
	filenames = []
	for bb in buffer:
		filenames.append('run'+str(int(bb[0])).zfill(2)+'_q_'+str(bb[1]))
	return filenames

##############################################
class File:
	
	#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	def __init__(self, path, mode='r'):
		self.mode_options = ['r', 'w', 'w+', 'a']
		self.open(path, mode=mode)
		pass
	
	#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	def open(self, path, mode):
		if mode in self.mode_options:
			try: 
				self.file = h5py.File(path, mode)
				self.flag = 1
			except IOError:
				print "Could not open file."
				self.flag = 0
		else:
			raise Exception, "File action not recognized."
		
	#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	def isopen(self):
		if self.flag == 1:
			return True
		else:
			return False
	
	#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	def close(self):
		self.file.close()
		self.flag = 0
		pass
	

##############################################
class Waveforms(File):

	#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	def __init__(self, path, mode='r'):
		 self.path = path
		 File.__init__(self, path, mode=mode) 
		 
	#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	def time(self, l, m, subdir=''):
		if self.isopen():
			return self.file[subdir+'Y_l'+str(l)+'_m'+str(m)+'.dat'][:,0]	
		
	#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	def amp(self, l, m, subdir=''):
		return self.file[subdir+'Y_l'+str(l)+'_m'+str(m)+'.dat'][:,1]
		#return self.file[subdir+'Amp/['+str(l)+','+str(m)+']'][:]
	
	#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	def amps(self, modes, subdir=''):
		ans = []
		for lm in modes:
			l, m = lm
			ans.append(self.amp(l, m, subdir=subdir))
		return np.array(ans)
	
	#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	def phase(self, l, m, subdir=''):
		return self.file[subdir+'Y_l'+str(l)+'_m'+str(m)+'.dat'][:,2]
		#return self.file[subdir+'Phase/['+str(l)+','+str(m)+']'][:]
		
	#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	def phases(self, modes, subdir=''):
		ans = []
		for lm in modes:
			l, m = lm
			ans.append(self.phase(l, m, subdir=subdir))
		return np.array(ans)
	
	#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	def hplus(self, l, m, subdir=''):
		return self.file[subdir+'Y_l'+str(l)+'_m'+str(m)+'.dat'][:,1]
		#return self.file[subdir+'hplus/['+str(l)+','+str(m)+']'][:]

	#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	def hcross(self, l, m, subdir=''):
		return self.file[subdir+'Y_l'+str(l)+'_m'+str(m)+'.dat'][:,2]
		#return self.file[subdir+'hcross/['+str(l)+','+str(m)+']'][:]
	
	#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	def waveform(self, l, m, subdir=''):
		amp = self.amp(l, m, subdir=subdir)
		phase = self.phase(l, m, subdir=subdir)
		return amp*np.exp(1j*phase)
		
	
