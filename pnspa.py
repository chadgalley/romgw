"""    
    Post-Newtonian inspiral waveforms in the frequency domain from the stationary
    phase approximation. Masses are assumed to be in seconds.
"""

import numpy as np
import gwtools


class PrecomputeFrequencies:
  """Precompute frequencies for faster evaluation times"""
  
  def __init__(self, samples):
    self.fm7over6 = samples**(-7./6.)
    self.fm5over3 = samples**(-5./3.)
    self.fm3over3 = samples**(-1.)
    self.fm2over3 = samples * self.fm5over3
    self.fm1over3 = samples**(-1./3.)
    self.fp1over3 = 1.0 / self.fm1over3
    self.fp2over3 = 1.0 / self.fm2over3
    #self.logf = adipy.log(samples)	
    self.logf = np.log(samples)	


class Amplitude(PrecomputeFrequencies):
  """PN waveform amplitude for non-spinning binary inspirals"""
  
  def __init__(self, samples=None):
    if samples is None:
      self.switch_dict = {
        '0': self.amp_nospin_0d0
        }
    else:
      PrecomputeFrequencies.__init__(self, samples)
      self.switch_dict = {
        '0': self.amp_nospin_0d0_pre
        }
    self.options = self.switch_dict.keys()	
    
  def __call__(self, args, pn_type):
    return self.switch_dict[pn_type](*args)
  
  
  #=========================================
  # PN amplitudes using input frequencies
    
  def amp_nospin_0d0(self, Mc, f):
    """0PN waveform amplitude for non-spinning binary inspirals"""
    return np.sqrt(5./24.)*np.pi**(-2./3.)*gwtools.c/gwtools.Mpcinm*Mc**(5./6.)*f**(-7./6.)
    
  
  #=========================================
  # PN amplitudes using precomputed frequency powers
  
  def amp_nospin_0d0_pre(self, Mc):
    """0PN waveform amplitude for non-spinning binary inspirals using pre-computed frequency powers"""
    return np.sqrt(5./24.)*np.pi**(-2./3.)*gwtools.c/gwtools.Mpcinm*Mc**(5./6.)*self.fm7over6


class Phase(PrecomputeFrequencies):
  """Class to compute phases of post-Newtonian restricted waveforms in the stationary phase approximation for non-spinning inspirals"""
  
  def __init__(self, samples=None):
    if samples is None:
      self.switch_dict = {
        '0': self.phase_nospin_0d0,
        '1': self.phase_nospin_1d0,
        '1.5': self.phase_nospin_1d5,
        '2': self.phase_nospin_2d0,
        '2.5': self.phase_nospin_2d5,
        '3': self.phase_nospin_3d0,
        '3.5': self.phase_nospin_3d5
        }
    else:
      PrecomputeFrequencies.__init__(self, samples)
      self.switch_dict = {
        '0': self.phase_nospin_0d0_pre,
        '1': self.phase_nospin_1d0_pre,
        '1.5': self.phase_nospin_1d5_pre,
        '2': self.phase_nospin_2d0_pre,
        '2.5': self.phase_nospin_2d5_pre,
        '3': self.phase_nospin_3d0_pre,
        '3.5': self.phase_nospin_3d5_pre
        }
    self.options = self.switch_dict.keys()
    
  def __call__(self, args, pn_type):
    return self.switch_dict[pn_type](*args)
  
  
  #=========================================
  # PN phases using input frequencies
    
  def phase_nospin_0d0(self, Mc, f):
    """0PN waveform phase for non-spinning binary inspirals"""
    return 3./128.*(np.pi*Mc*f)**(-5./3.)
  
  def phase_nospin_1d0(self, Mc, nu, f):
    """1PN waveform phase for non-spinning binary inspirals"""
    Mtot = gwtools.Mcnu_to_M(Mc, nu)
    c2 = 3./128./nu/(np.pi*Mtot)*(3715.0/756.0+55.0/9.0*nu)
    return self.phase_nospin_0d0(Mc,f) + c2*f**(-1.)
  
  def phase_nospin_1d5(self, Mc, nu, f):
    """1.5PN waveform phase for non-spinning binary inspirals"""
    Mtot = gwtools.Mcnu_to_M(Mc, nu)
    c3 = 3./128./nu/(np.pi*Mtot)**(2./3.)*(-16.0*np.pi)
    return self.phase_nospin_1d0(Mc,nu,f) + c3*f**(-2./3.)
  
  def phase_nospin_2d0(self, Mc, nu, f):
    """2PN waveform phase for non-spinning binary inspirals"""
    Mtot = gwtools.Mcnu_to_M(Mc, nu)
    c4 = 3./128./nu/(np.pi*Mtot)**(1./3.) \
        * (15293365.0/508032.0+27145.0/504.0*nu+3085.0/72.0*nu**2)
    return self.phase_nospin_1d5(Mc,nu,f) + c4*f**(-1./3.)
  
  def phase_nospin_2d5(self, Mc, nu, f, v_lso=1.):
    """2.5PN waveform phase for non-spinning binary inspirals"""
    Mtot = gwtools.Mcnu_to_M(Mc, nu)
    #v_lso = 6.0**(-0.5)
    c5 = 3./128./nu*np.pi*(38645.0/756.0-65.0/9.0*nu) \
        * ( 1.0 + np.log(v_lso**(-3.)*np.pi*Mtot) + np.log(f) )
    return self.phase_nospin_2d0(Mc,nu,f) + c5
  
  def phase_nospin_3d0(self, Mc, nu, f, v_lso=1.):
    """3PN waveform phase for non-spinning binary inspirals"""
    Mtot = gwtools.Mcnu_to_M(Mc, nu)
    c6 = 3./128./nu*(np.pi*Mtot)**(1./3.)*(11583231236531.0/4694215680.0 \
      - 640.0/3.0*np.pi**2 - 6848.0/21.0*(gwtools.gamma_E + 1./3.*np.log(64.0*np.pi*Mtot)\
      + 1./3.*np.array(np.log(f))) - 15737765635.0/3048192.0*nu \
      + 2255.0/12.0*np.pi**2*nu + 76055.0/1728.0*nu**2 \
      - 127825.0/1296.0*nu**3 )
    return self.phase_nospin_2d5(Mc,nu,f,v_lso) + c6*f**(1./3.)
  
  def phase_nospin_3d5(self, Mc, nu, f, v_lso=1.):
    """3.5PN waveform phase for non-spinning binary inspirals"""
    Mtot = gwtools.Mcnu_to_M(Mc, nu)
    c7 = 3./128./nu*(np.pi*Mtot)**(2./3.) * np.pi * ( 77096675.0/254016.0 \
      + 378515.0/1512.0*nu - 74045.0/756.0*nu**2 )
    return self.phase_nospin_3d0(Mc,nu,f,v_lso) + c7*f**(2./3.)
  
  
  #=========================================
  # PN phases using precomputed frequency powers
  	
  def phase_nospin_0d0_pre(self, Mc):
    """0PN waveform phase for non-spinning binary inspirals using pre-computed frequency powers"""
    return 3./128.*(np.pi*Mc)**(-5./3.)*self.fm5over3
  
  def phase_nospin_1d0_pre(self, Mc, nu):
    """1PN waveform phase for non-spinning binary inspirals using pre-computed frequency powers"""
    Mtot = gwtools.Mcnu_to_M(Mc, nu)
    c2 = 3./128./nu/(np.pi*Mtot)*(3715.0/756.0+55.0/9.0*nu)
    return self.phase_nospin_0d0_pre(Mc) + c2*self.fm3over3
  
  def phase_nospin_1d5_pre(self, Mc, nu):
    """1.5PN waveform phase for non-spinning binary inspirals using pre-computed frequency powers"""
    Mtot = gwtools.Mcnu_to_M(Mc, nu)
    c3 = 3./128./nu/(np.pi*Mtot)**(2./3.)*(-16.0*np.pi)
    return self.phase_nospin_1d0_pre(Mc,nu) + c3*self.fm2over3
  
  def phase_nospin_2d0_pre(self, Mc, nu):
    """2PN waveform phase for non-spinning binary inspirals using pre-computed frequency powers"""
    Mtot = gwtools.Mcnu_to_M(Mc, nu)
    c4 = 3./128./nu/(np.pi*Mtot)**(1./3.) \
        * (15293365.0/508032.0+27145.0/504.0*nu+3085.0/72.0*nu**2)
    return self.phase_nospin_1d5_pre(Mc,nu) + c4*self.fm1over3
  
  def phase_nospin_2d5_pre(self, Mc, nu, v_lso=1.):
    """2.5PN waveform phase for non-spinning binary inspirals using pre-computed frequency powers"""
    Mtot = gwtools.Mcnu_to_M(Mc, nu)
    #v_lso = 6.0**(-0.5)
    c5 = 3./128./nu*np.pi*(38645.0/756.0-65.0/9.0*nu) \
      * ( 1.0 + np.log(v_lso**(-3.)*np.pi*Mtot) + self.logf )
    return self.phase_nospin_2d0_pre(Mc,nu) + c5
  
  def phase_nospin_3d0_pre(self, Mc, nu, v_lso=1.):
    """3PN waveform phase for non-spinning binary inspirals using pre-computed frequency powers"""
    Mtot = gwtools.Mcnu_to_M(Mc, nu)
    c6 = 3./128./nu*(np.pi*Mtot)**(1./3.)*(11583231236531.0/4694215680.0 \
      - 640.0/3.0*np.pi**2 - 6848.0/21.0*(gwtools.gamma_E + 1./3.*np.log(64.0*np.pi*Mtot)\
      + 1./3.*self.logf) - 15737765635.0/3048192.0*nu \
      + 2255.0/12.0*np.pi**2*nu + 76055.0/1728.0*nu**2 \
      - 127825.0/1296.0*nu**3 )
    return self.phase_nospin_2d5_pre(Mc,nu,v_lso=v_lso) + c6*self.fp1over3
  
  def phase_nospin_3d5_pre(self, Mc, nu, v_lso=1.):
    """3.5PN waveform phase for non-spinning binary inspirals using pre-computed frequency powers"""
    Mtot = gwtools.Mcnu_to_M(Mc, nu)
    c7 = 3./128./nu*(np.pi*Mtot)**(2./3.) * np.pi * ( 77096675.0/254016.0 \
      + 378515.0/1512.0*nu - 74045.0/756.0*nu**2 )
    return self.phase_nospin_3d0_pre(Mc,nu,v_lso=v_lso) + c7*self.fp2over3


class TaylorF2(PrecomputeFrequencies, Amplitude, Phase):
  """Post-Newtonian restricted waveforms in the stationary phase approximation for non-spinning inspirals"""
  
  def __init__(self, samples=None):
    Amplitude.__init__(self, samples)
    self.amp = Amplitude(samples)
    Phase.__init__(self, samples)
    if samples is None:
      self.switch_dict = {
        '0': self.nospin_0d0,
        '1': self.nospin_1d0,
        '1.5': self.nospin_1d5,
        '2': self.nospin_2d0,
        '2.5': self.nospin_2d5,
        '3': self.nospin_3d0,
        '3.5': self.nospin_3d5
        }		
    else:
      PrecomputeFrequencies.__init__(self, samples)
      self.switch_dict = {
        '0': self.nospin_0d0_pre,
        '1': self.nospin_1d0_pre,
        '1.5': self.nospin_1d5_pre,
        '2': self.nospin_2d0_pre,
        '2.5': self.nospin_2d5_pre,
        '3': self.nospin_3d0_pre,
        '3.5': self.nospin_3d5_pre
        }		
    self.options = self.switch_dict.keys()
  
  def __call__(self, args, pn_type):
    return self.switch_dict[pn_type](*args)
  
    
  #=========================================
  # Restricted PN waveforms using input frequencies
    
  def nospin_0d0(self, Mc, f):
    """0PN restricted waveform for non-spinning binary inspirals"""
    return self.amp([Mc,f],'0')*np.exp(-1j*np.pi/4.+ 1j*self.phase_nospin_0d0(Mc,f))
  
  def nospin_1d0(self, Mc, nu, f):
    """1PN restricted waveform for non-spinning binary inspirals"""
    return self.amp([Mc,f],'0')*np.exp(-1j*np.pi/4.+ 1j*self.phase_nospin_1d0(Mc,nu,f))
  
  def nospin_1d5(self, Mc, nu, f):
    """1.5PN restricted waveform for non-spinning binary inspirals"""
    return self.amp([Mc,f],'0')*np.exp(-1j*np.pi/4.+ 1j*self.phase_nospin_1d5(Mc,nu,f))
  
  def nospin_2d0(self, Mc, nu, f):
    """2PN restricted waveform for non-spinning binary inspirals"""
    return self.amp([Mc,f],'0')*np.exp(-1j*np.pi/4.+ 1j*self.phase_nospin_2d0(Mc,nu,f))
  
  def nospin_2d5(self, Mc, nu, f, v_lso=1.):
    """2.5PN restricted waveform for non-spinning binary inspirals"""
    return self.amp([Mc,f],'0')*np.exp(-1j*np.pi/4.+ 1j*self.phase_nospin_2d5(Mc,nu,f,v_lso))
  
  def nospin_3d0(self, Mc, nu, f, v_lso=1.):
    """3PN restricted waveform for non-spinning binary inspirals"""
    return self.amp([Mc,f],'0')*np.exp(-1j*np.pi/4.+ 1j*self.phase_nospin_3d0(Mc,nu,f,v_lso))
  
  def nospin_3d5(self, Mc, nu, f, v_lso=1.):
    """3.5PN restricted waveform for non-spinning binary inspirals"""
    return self.amp([Mc,f],'0')*np.exp(-1j*np.pi/4.+ 1j*self.phase_nospin_3d5(Mc,nu,f,v_lso))
  
  
  #=========================================
  # Restricted PN waveforms using precomputed frequency powers
  
  def nospin_0d0_pre(self, Mc):
    """0PN restricted waveform for non-spinning binary inspirals"""
    return self.amp([Mc],'0')*np.exp(-1j*np.pi/4.+ 1j*self.phase_nospin_0d0_pre(Mc))
  
  def nospin_1d0_pre(self, Mc, nu):
    """1PN restricted waveform for non-spinning binary inspirals"""
    return self.amp([Mc],'0')*np.exp(-1j*np.pi/4.+ 1j*self.phase_nospin_1d0_pre(Mc,nu))
  
  def nospin_1d5_pre(self, Mc, nu):
    """1.5PN restricted waveform for non-spinning binary inspirals"""
    return self.amp([Mc],'0')*np.exp(-1j*np.pi/4.+ 1j*self.phase_nospin_1d5_pre(Mc,nu))
  
  def nospin_2d0_pre(self, Mc, nu):
    """2PN restricted waveform for non-spinning binary inspirals"""
    return self.amp([Mc],'0')*np.exp(-1j*np.pi/4.+ 1j*self.phase_nospin_2d0_pre(Mc,nu))
  
  def nospin_2d5_pre(self, Mc, nu, v_lso=1.):
    """2.5PN restricted waveform for non-spinning binary inspirals"""
    return self.amp([Mc],'0')*np.exp(-1j*np.pi/4.+ 1j*self.phase_nospin_2d5_pre(Mc,nu,v_lso))
  
  def nospin_3d0_pre(self, Mc, nu, v_lso=1.):
    """3PN restricted waveform for non-spinning binary inspirals"""
    return self.amp([Mc],'0')*np.exp(-1j*np.pi/4.+ 1j*self.phase_nospin_3d0_pre(Mc,nu,v_lso))
  
  def nospin_3d5_pre(self, Mc, nu, v_lso=1.):
    """3.5PN restricted waveform for non-spinning binary inspirals"""
    return self.amp([Mc],'0')*np.exp(-1j*np.pi/4.+ 1j*self.phase_nospin_3d5_pre(Mc,nu,v_lso))
