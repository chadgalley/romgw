# --- psd.py ---

"""    
    Pseudo-spectral densities (PSDs) of various gravitational wave detectors.
    Includes white noise PSD.
"""

__author__ = "Chad Galley <crgalley@gmail.com>"

import numpy as np


#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
def iligo(samples):
	"""PSD for Initial LIGO"""
	f0 = 150.
	x = samples/f0
	return 9.e-46 * ((4.49*x)**(-56.) + 0.16*x**(-4.52) + 0.52 + 0.32*x**2.)

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
def eligo(samples):
	"""PSD for Enhanced LIGO"""
	f0 = 178.
	x = samples/f0
	return 1.5e-46 * (1.33e-27*np.exp(-5.5*(np.log(x))**2)*x**(-52./6.) \
			+ 0.16*x**(-4.2) + 0.52 + 0.3*x**2.1)

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
def aligo(samples):
	"""PSD for Advanced LIGO"""
	f0 = 215.
	x = samples/f0
	return 1e-49*(x**(-4.14) - 5.*x**(-2.) \
			+ 111.*(1.-x**2.+1./2.*x**4.)/(1.+x**2./2.))

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
def aligo_zdhp(samples):
	"""PSD for Advanced LIGO, zero-detuned, high power"""
	f0 = 245.4
	x = samples/f0
	return 1e-48*(0.0152*x**(-4.) + 0.2935*x**(9./4.) + 2.7951*x**(3./2.) \
			- 6.5080*x**(3./4.) + 17.7622)

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
def avirgo(samples):
	"""PSD for Advanced VIRGO"""
	f0 = 245.4
	x = samples/f0
	return 1e-47*(2.67e-7*x**(-5.6) \
			+0.59*np.exp((np.log(x))**2.*(-3.2-1.08*np.log(x)-0.13*(np.log(x))**2))*x**(-4.1) \
			+0.68*np.exp(-0.73*(np.log(x))**2.)*x**5.34)

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
def white(samples):
	"""PSD for white noise"""
	return np.ones(len(samples))


